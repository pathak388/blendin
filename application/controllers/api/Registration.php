<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
		    $this->load->helper('api');
    }
    
    public function index()
    {
      echo 'index';
    }

    /**
     * APP OPEN (call when app starts)
     * @param   array
     * @return  Json Object
     */
    public function appOpen()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0' => 'device_id',
                            '1' => 'user_id',
                            '2' => 'msisdn',
                            '3' => 'app_open_for_day',
                            '4' => 'client_time'
                            );
     $resultJson    =  validateJson($requestJson, $check_request_keys);
     // print_r($request_data);
      //echo date('d-m-Y h:i:s', time());
      if($resultJson==1)
      {

        $userMaster['species']    = trim($requestJson['blendin']['device_id']); 
        $userMaster['family']     = trim($requestJson['blendin']['user_id']); 
        $userMaster['genus']      = trim($requestJson['blendin']['msisdn']); 
        $userMaster['client_time']= trim($requestJson['blendin']['client_time']); 
        $userMaster['vi']         = trim($requestJson['blendin']['app_open_for_day']); 
        $userMaster['add_date']   = strtotime(date('d-m-Y h:i:s'));

        # Validate is Fields not blank
        isBlank($userMaster['species'], '0', '133'); 
        isBlank($userMaster['vi'], '0', '134');

        $this->api_model->appStart($userMaster);
      }
      else
      {
        generateServerResponse('0','100');
      } 
    }

    /**
     * REGISTER USER
     * @param   array
     * @return  Json Object
     */
    public function register()
    {
     $request_data        = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson         = json_decode($request_data,true);  
     $check_request_keys  = array(
                            '0'   => 'name',
                            '1'   => 'mobile',
                            '2'   => 'email',
                            '3'   => 'date_of_birth',
                            '4'   => 'gender',
                            '5'   => 'about_me',
                            '6'   => 'interest_list',
                            '7'   => 'cuisine_list',
                            '8'   => 'foodi_type',
                            '9'   => 'education',
                            '10'  => 'work',
                            '11'  => 'device_id'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      {
        $userMaster['name']         = trim($requestJson['blendin']['name']); 
        $userMaster['mobile']       = trim($requestJson['blendin']['mobile']); 
        $userMaster['email']        = trim($requestJson['blendin']['email']); 
        $userMaster['date_of_birth']= trim($requestJson['blendin']['date_of_birth']); 
        $userMaster['gender']       = trim($requestJson['blendin']['gender']); 
        $userMaster['about_me']     = trim($requestJson['blendin']['about_me']); 
        $userMaster['foodi_type']   = trim($requestJson['blendin']['foodi_type']); 
        $userMaster['education']    = trim($requestJson['blendin']['education']); 
        $userMaster['work']         = trim($requestJson['blendin']['work']); 
        $userMaster['species']      = trim($requestJson['blendin']['device_id']);
        $userMaster['interest_list']=      $requestJson['blendin']['interest_list'];
        $userMaster['cuisine_list'] =      $requestJson['blendin']['cuisine_list']; 

        # Validate is Fields not blank
        isBlank($userMaster['species'], '0', '133'); 
        // $isExists = $this->api_model->isExists('user', 'mobile', $userMaster['mobile']); 
        $isExists = $this->api_model->is_user_registered($userMaster['email'], $userMaster['mobile']); 
        if($isExists==1)
        {
          // generateServerResponse('0','135');
          // $this->api_model->update_user($userMaster); 
          $this->api_model->register($userMaster, 1); 
        }
        else
        {
          $this->api_model->register($userMaster, 0); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      }  
    }

    /** 
     * VERIFY REGISTERED MOBILE (MSISDN)
     * @param   array
     * @return  Json Object
     */
    public function verifyiMsisdn()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'msisdn',
                            '1'   => 'device_id'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      {
 
        $userMaster['mobile']         = trim($requestJson['blendin']['msisdn']);  
        $userMaster['device_id']      = trim($requestJson['blendin']['device_id']); 

        # Validate is Fields not blank
        isBlank($userMaster['device_id'], '0', '133'); 
        isBlank($userMaster['mobile'],    '0', '138'); 
        // $isExists = $this->api_model->isExists('user', 'mobile', $userMaster['mobile']);
        $isExists = $this->api_model->validate_user_device($userMaster);

        if($isExists==1)
        {
          $this->api_model->sendOtp($userMaster);
        }
        else
        {
          generateServerResponse('0','141');
        }
      }
      else
      {
        generateServerResponse('0','100');
      }  
    }

    /** 
     * VERIFY REGISTERED MOBILE (MSISDN)
     * @param   array
     * @return  Json Object
     */
    public function verfyiOtp()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'otp',
                            '1'   => 'msisdn',
                            '2'   => 'device_id'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      {
        $userMaster['otp']        = trim($requestJson['blendin']['otp']);  
        $userMaster['mobile']     = trim($requestJson['blendin']['msisdn']);  
        $userMaster['device_id']  = trim($requestJson['blendin']['device_id']); 

        # Validate is Fields not blank
        isBlank($userMaster['otp'],       '0', '139'); 
        isBlank($userMaster['device_id'], '0', '133'); 
        isBlank($userMaster['mobile'],    '0', '138'); 
        $isExists = $this->api_model->isExists('user', 'mobile', $userMaster['mobile']);

        if($isExists==1)
        {
          $this->api_model->verifyOtp($userMaster);
        }
        else
        {
          generateServerResponse('0','135');
        }
      }
      else
      {
        generateServerResponse('0','100');
      }  
    }

    /** 
     * VERIFY REGISTERED MOBILE (MSISDN)
     * @param   array
     * @return  Json Object
     */
    public function signUpAttempt()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'msisdn',
                            '1'   => 'device_id'
                          );
      $resultJson   =  validateJson($requestJson, $check_request_keys); 
      if($resultJson==1)
      {   
        $userMaster['mobile']   = trim($requestJson['blendin']['msisdn']);  
        $userMaster['species']  = trim($requestJson['blendin']['device_id']); 

        # Validate is Fields not blank 
        isBlank($userMaster['species'],   '0', '133'); 
        isBlank($userMaster['mobile'],    '0', '138'); 

        $this->api_model->signUpAttempt($userMaster);
        // $isExists = $this->api_model->isExists('user', 'mobile', $userMaster['mobile']);
        // if($isExists==1) { $this->api_model->signUpAttempt($userMaster); }
        // else { generateServerResponse('0','135'); }
      }
      else
      {
        generateServerResponse('0','100');
      } 
    }

    
 
}
