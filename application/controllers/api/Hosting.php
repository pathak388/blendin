<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Hosting extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
		    $this->load->helper('api');
    }
    
    public function index(){}

    /**
     * POST EVENT
     * @param   array
     * @return  Json Object
     */     
    public function postEvent()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'user_id',
                            '1'   => 'device_id',
                            '2'   => 'event_photo_count',
                            '3'   => 'title',
                            '4'   => 'event_type',
                            '5'   => 'about_event',
                            '6'   => 'tags',
                            '7'   => 'interest_list',
                            '8'   => 'cuisine_list',
                            '9'   => 'guest_list',
                            '10'  => 'drink_list',
                            '11'  => 'about_menu',
                            '12'  => 'date',
                            '13'  => 'timeslot',
                            '14'  => 'location',
                            '15'  => 'address',
                            '16'  => 'cost',
                            '17'  => 'max_guest',
                            '18'  => 'min_guest',
                            '19'  => 'cooked_by',
                            '20'  => 'guest_gender_id'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      { 
        $userMaster['device_id']          = trim($requestJson['blendin']['device_id']); 
        $userMaster['user_id']            = trim($requestJson['blendin']['user_id']); 
        // $userMaster['auth']            = trim($requestJson['blendin']['auth']);
        $userMaster['image_count']        = trim($requestJson['blendin']['event_photo_count']); 
        $userMaster['title']              = trim($requestJson['blendin']['title']); 
        $userMaster['event_type']         = trim($requestJson['blendin']['event_type']); 
        $userMaster['description']        = trim($requestJson['blendin']['about_event']);
        $userMaster['about_menu']         = trim($requestJson['blendin']['about_menu']); 
        $userMaster['date']               = trim($requestJson['blendin']['date']); 
        $userMaster['time']               = trim($requestJson['blendin']['timeslot']); 
        $userMaster['venue_id']           = trim($requestJson['blendin']['location']); 
        $userMaster['cost']               = trim($requestJson['blendin']['cost']); 
        $userMaster['max_guest']          = trim($requestJson['blendin']['max_guest']); 
        $userMaster['min_guest']          = trim($requestJson['blendin']['min_guest']); 
        $userMaster['cooked_by']          = trim($requestJson['blendin']['cooked_by']); 
        $userMaster['guest_gender_id']    = trim($requestJson['blendin']['guest_gender_id']);  
        $userMaster['cuisine_type']       = trim($requestJson['blendin']['cuisine_type']);  
        $userMaster['tag']                =      $requestJson['blendin']['tags']; 
        $userMaster['interest_list']      =      $requestJson['blendin']['interest_list']; 
        $userMaster['cuisine_list']       =      $requestJson['blendin']['cuisine_list']; 
        $userMaster['guest_list']         =      $requestJson['blendin']['guest_list']; 
        $userMaster['drink_list']         =      $requestJson['blendin']['drink_list']; 

        //Validate is Fields not blank
        //isBlank($userMaster['auth'],        '0', '140'); 
        isBlank($userMaster['device_id'],     '0', '133'); 
        isBlank($userMaster['user_id'],       '0', '125'); 
        isBlank($userMaster['event_type'],    '0', '142'); 
        
        # CHECK WHETHER event_type IN constant TABLE EXISTS OR NOT
        if(!$this->api_model->isExists('constant', 'type', $userMaster['event_type']))
        {
          generateServerResponse('0','143');
        }
        // $isExists = $this->api_model->isExists('user', '_id', $userMaster['user_id']);
        $isAuth   = isAuthoriesd($userMaster['user_id'], $userMaster['device_id']);

        // if($isExists==1)
        if($isAuth==1)
        {  
          $this->api_model->postEvent($userMaster);
        }
        else
        {
          generateServerResponse('0','141'); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      }  
    }
    
 
}
