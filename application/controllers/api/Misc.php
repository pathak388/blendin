<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Misc extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
		    $this->load->helper('api');
    }
    
    public function index(){}

    /**
     * UPLOAD PROFILE PICTURE
     * @param   array
     * @return  Json Object
     */
    public function getImage()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'user_id',
                            '1'   => 'device_id',
                            '2'   => 'auth',
                            '3'   => 'image_type',      // image folder name (eg. small, medium, high, thumbnail)
                            '4'   => 'object_id'        // image name of user
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      { 
        $userMaster['device_id']    = trim($requestJson['blendin']['device_id']); 
        $userMaster['user_id']      = trim($requestJson['blendin']['user_id']); 
        $userMaster['auth']         = trim($requestJson['blendin']['auth']);
        $userMaster['image_type']   = trim($requestJson['blendin']['image_type']); 
        $userMaster['object_id']    = trim($requestJson['blendin']['object_id']); 

        //Validate is Fields not blank
        isBlank($userMaster['auth'],      '0', '140'); 
        isBlank($userMaster['device_id'], '0', '133'); 
        isBlank($userMaster['user_id'],   '0', '125'); 
        
        $isExists = $this->api_model->isExists('user', '_id', $userMaster['user_id']);

        if($isExists==1)
        {  
          $images = $this->api_model->get_user_pic($userMaster['user_id'], $userMaster['image_type']);
          // $data['data'] = $images;
          // generateServerResponse('1','S', $data); 
        }
        else
        {
          generateServerResponse('0','141'); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      } 
      // generateServerResponse('0','119');
    }
 
}
