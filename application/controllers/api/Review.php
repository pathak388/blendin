<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Review extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
		    $this->load->helper('api');
    }
    
    public function index(){}

    /**
     * SUBMIT REVIEW GUEST
     * @param   array
     * @return  Json Object
     */
    public function submitReviewGuest()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'user_id',
                            '1'   => 'device_id',
                            '2'   => 'auth',
                            '3'   => 'event_id',  
                            '4'   => 'was_attended',       
                            '5'   => 'message',
                            '6'   => 'review',
                            '7'   => 'experience',
                            '8'   => 'food_quality',
                            '9'   => 'meet_again_list'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      { 
        $auth                       = trim($requestJson['blendin']['auth']);
        $device_id                  = trim($requestJson['blendin']['device_id']); 
        $userMaster['guest_id']     = trim($requestJson['blendin']['user_id']); 
        $userMaster['_id']          = trim($requestJson['blendin']['event_id']); 
        $userMaster['was_attended'] = trim($requestJson['blendin']['was_attended']); 
        $userMaster['message']      = trim($requestJson['blendin']['message']); 
        $userMaster['review']       = trim($requestJson['blendin']['review']); 
        $userMaster['rating']       = trim($requestJson['blendin']['experience']); 
        $userMaster['meal_rating']  = trim($requestJson['blendin']['food_quality']); 
        $userMaster['meet_again_list']= get_array_to_json_string($requestJson['blendin']['meet_again_list']); 

        //Validate is Fields not blank
        // isBlank($userMaster['auth'],      '0', '140'); 
        isBlank($device_id,               '0', '133'); 
        isBlank($userMaster['guest_id'],   '0', '125'); 
        isBlank($userMaster['_id'],  '0', '145');  
        
        $Auth = isAuthoriesd($userMaster['guest_id'], $device_id);

        if($Auth==1)
        {  
          $images = $this->api_model->submit_review_guest($userMaster); 
        }
        else
        {
          generateServerResponse('0','141'); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      } 
      // generateServerResponse('0','119');
    }

    /**
     * SUBMIT REVIEW HOST
     * @param   array
     * @return  Json Object
     */
    public function submitReviewHost()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'user_id',
                            '1'   => 'device_id',
                            '2'   => 'auth',
                            '3'   => 'event_id',  
                            '4'   => 'was_hosted',       
                            '5'   => 'message',
                            '6'   => 'review',
                            '7'   => 'host_again_type', 
                            '8'   => 'venue_id',
                            '9'   => 'meet_again_list'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      { 
        $auth                         = trim($requestJson['blendin']['auth']);
        $device_id                    = trim($requestJson['blendin']['device_id']); 
        $userMaster['guest_id']       = trim($requestJson['blendin']['user_id']); 
        $userMaster['_id']            = trim($requestJson['blendin']['event_id']); 
        $userMaster['was_hosted']     = trim($requestJson['blendin']['was_hosted']); 
        $userMaster['message']        = trim($requestJson['blendin']['message']); 
        $userMaster['review']         = trim($requestJson['blendin']['review']); 
        $userMaster['host_again_type']= trim($requestJson['blendin']['host_again_type']); 
        // $userMaster['venue_id']    = trim($requestJson['blendin']['venue_id']); 
        $userMaster['meet_again_list']= get_array_to_json_string($requestJson['blendin']['meet_again_list']); 

        //Validate is Fields not blank
        // isBlank($userMaster['auth'],     '0', '140'); 
        isBlank($device_id,                 '0', '133'); 
        isBlank($userMaster['guest_id'],    '0', '125'); 
        isBlank($userMaster['_id'],         '0', '145');  
        
        $Auth = isAuthoriesd($userMaster['guest_id'], $device_id);

        if($Auth==1)
        {  
          $images = $this->api_model->submit_review_host($userMaster); 
        }
        else
        {
          generateServerResponse('0','141'); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      } 
      // generateServerResponse('0','119');
    }
 
}
