<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
		    $this->load->helper('api');
    }
    
    public function index(){}

    /**
     * UPLOAD PROFILE PICTURE
     * @param   base64 string
     * @return  Json Object
     */
    public function uploadProfilePicture()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'file',
                            '1'   => 'device_id',
                            '2'   => 'user_id'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      {

        $userMaster['file']         = trim($requestJson['blendin']['file']); 
        $userMaster['device_id']    = trim($requestJson['blendin']['device_id']); 
        $userMaster['user_id']      = trim($requestJson['blendin']['user_id']);  

        //Validate is Fields not blank
        isBlank($userMaster['file'],      '0', '133'); 
        isBlank($userMaster['device_id'], '0', '133'); 
        isBlank($userMaster['user_id'],   '0', '125'); 
        
        $isExists = $this->api_model->isExists('user', '_id', $userMaster['user_id']);

        if($isExists==1)
        {
          //$userMaster['file'] = "data:image/png;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/7QCcUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAIAcAmcAFFpzRk9IQ3BBQzBFQjZYejZnMHpwHAIoAGJGQk1EMDEwMDBhYmYwMzAwMDA5YzA1MDAwMDVhMDgwMDAwZDEwODAwMDA0OTA5MDAwMGVmMGEwMDAwNGIwZTAwMDBjNDBlMDAwMDVjMGYwMDAwZjYwZjAwMDA5MzE1MDAwMP/iAhxJQ0NfUFJPRklMRQABAQAAAgxsY21zAhAAAG1udHJSR0IgWFlaIAfcAAEAGQADACkAOWFjc3BBUFBMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD21gABAAAAANMtbGNtcwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmRlc2MAAAD8AAAAXmNwcnQAAAFcAAAAC3d0cHQAAAFoAAAAFGJrcHQAAAF8AAAAFHJYWVoAAAGQAAAAFGdYWVoAAAGkAAAAFGJYWVoAAAG4AAAAFHJUUkMAAAHMAAAAQGdUUkMAAAHMAAAAQGJUUkMAAAHMAAAAQGRlc2MAAAAAAAAAA2MyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHRleHQAAAAARkIAAFhZWiAAAAAAAAD21gABAAAAANMtWFlaIAAAAAAAAAMWAAADMwAAAqRYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9jdXJ2AAAAAAAAABoAAADLAckDYwWSCGsL9hA/FVEbNCHxKZAyGDuSRgVRd13ta3B6BYmxmnysab9908PpMP///9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8IAEQgAoACgAwAiAAERAQIRAf/EABwAAAEFAQEBAAAAAAAAAAAAAAUAAgMEBgEHCP/EABgBAAMBAQAAAAAAAAAAAAAAAAABAgME/8QAGAEAAwEBAAAAAAAAAAAAAAAAAAECAwT/2gAMAwAAARECEQAAAcS21JDp2GwDIa7z+wP2Rvl8svVRZywq0pPH22tdyO24rNt8Cve4T0lz0g8HoGa0WOdM1zyau+NZJe8TkTK47EgiWo2Wq8yOOfSHh9E1HZ46pSSDwp9avNlhhGkqGqByLDppc9azyE86UpSTkACdShvy7j0vx/1epupKpSSDxzutdU5Qafiz18/1rtPhvnQpwIrYeHaabERjmVndDHhWuGl9WBntMkkmJJBgLWRNNVS4Q9nsO0IYzz9FUXoBK0RAUQDMUNgqgDS1+e0w9Se122CSQKKWIPDCQfiejLZqNXoDmQ0fNvPHSpTpfE2aVO4aE20rArke/N6TeAaDXHnUg5FMg8A7EgL06MKeovuDc3WaIhy01ShsVi6xQZUrGMiFJ9HPstl556HUpJCSSD514xNRqPifp4ywuHuz9x46kYgFcpU4qNjp5L5APZpaX1rw7ctbpJS0kg+b2caJM7wZfWec3MdtnRErHcmEipbYdmhk2xsSVpBWzGfJUe138NuUJJJ/NDUg7zrQ5HI0GrvQ47jgdI14JcaE9+lba0/rnh/twOheOR//xAAoEAACAgIBAwMEAwEAAAAAAAABAgADBBESBRMgECExFCIjQTIzQkP/2gAIAQAAAQUC4woIqN6UZD1Ni9ZlN1d623V1Czqa8vrdxcqV2BvTU1NQCVpuKuvQKTNBYzzZM1K9yvkJxecIpAgMpsKvU+xqanGKkrGh6WtxBJacfXl7G33WwQMJz1O8BFvLHEusEoyAYBOMUeGo6bmtQ+03FcQFZ7TcewTe4jukrusJS736ffzGoPC1mSxHV4V3MgcZzgacjNtNEwV7i1TtzZqdLQ0wrOFi+48L/wC3UxubLmFXqiDkeAWLxgrBi1wJDXMxPtrMxX9sN+5j+FmDazDplkop7c6rjV0JrZx8Pghx69W46bqWwSv+Nl61wZtcyHWymv56TR3b1AVfBF+0T/v1TEWydLwx9TdX7WYyiJSFams8hX9uQx7lXbc20gU0rs9Ho7WL44963AfK/wB13x0wcU1sWU7hxNmuhUnD2z8c96mrgFpPZ6dVu9V4r4O2iNq2PfziH70bk1YCWqYz+11sfMoxx9YrJY4si1CPxVKvtinkvhZ8LkkSvIVivse5tMO3uoG9maJqXJXs2iY6lVRplN+IAzAblieG+QnAGYw41jIYDprjQMybO2KbjqzIimkRG4xTMyw90GdGflj+OodxD+PlqY4/Al2wrc2Lfa2SJZ27J2u229CyznkCdDbVvj7+jN7bmPXwxrPxNQytBppbigztcJY0ssLroz/PSX1mePKH0X5/xau591TJlz6oRsiX5JsYGKxldh1jW8bFIZfD9tB8bmNb3aHjiOoM4CX6roE/QPsjeyPOi5um8bP4+nT8rstzBjjcZZqZlvNxD8D4U+yts476ODf9RjeD/HrVkvXBnQ50tynf0EMX4HwplRnQL/yeB9T4/sfME/SxJ0uzhl+jHXp+/MfP79VEWY7aYHYMvbS//8QAIREAAgIBBAIDAAAAAAAAAAAAAAECERADEiAhEzEiQGH/2gAIAQIRAT8Bo9CKNo1xaKFm3xvC7FE2G1Evix5sSukacaGvwSok2anvhRDqSELDRqcLxF4plkvfGxKliyUqEPlRJ0hu8XxhqV0zyRNTU3dL6H//xAAhEQACAQQCAgMAAAAAAAAAAAAAAQIQESAhAzESMBMycf/aAAgBAREBPwH0LO1Fk2eR5EdoVbD1snK/Qv0kJEOsLk/rR0uQylpirDrCxYe3S4ouR1ncWxK1LYz477R8cjjhbbqh+p0//8QALxAAAQMDAgYBAgUFAAAAAAAAAQACERAhMQMgEiIwQVFhcWKBBDJCUqFAcpGxwf/aAAgBAAAGPwKnpXU6eo5pUfiGz9TVOk8OXO4BQAV3V1Yz1+Syklv+Vd4VnUt0cbPahXBX6la9YChcLrHoW3W/0srzTyslTeVHfce7VbZbf6o3c5f9VzKHCe9b7pq13nbLYg3V3NCcyZgppD5e44QA7rmyrlcr1Z6ur0dw0DThANsBtHxTV/uKa/8AVgBPc7DLD5oeKSjx/aKYTpGFdOLfFJIHE7dax8IrU+SuL9oRnJM1vTCLgMogNz5T25stPi8oAYG0KWfwsX8Jx90geaw1N43XQ4Yuh/KsnfCHooHzu52qzhPuktPNCGoc19qVDa/KwtP4jZhERB2RqM+7UWDsaWyubTfPwr6RaF3UdqMYMRRwPY7rU+yuUdRvY0vSCVhqEOJFOL3R7fI3XOxrfS+mnhZrDTXT926IrLMeFExTK5fy0zRhxBQLbg9Fjh4rehjJtUVGi88px66PC78hVtnCMNqEKtf3wdo2RkUwowOg7S/cJ2jr6R+r+kBQNCv/xAAmEAEAAgIBAwUAAwEBAAAAAAABABEhMUEQUXEgYYGRobHB8eHw/9oACAEAAAE/IY8CWxuu5hgztnjKbX+xMfXD8kqm+a2eSXT2zmIVJ9Qvx+liPBP2A2A99wyY6Ho55c1APv01Gu7Nrb3Z3mvfmZlYH7i+M+YMbGu25bG1zd1iP8iQWz9KFV9BKd1faWBQje56Fkg+/wBf7JwTSb7w5bYEN6gZZ+RMF+ILDC+XwldlPBOPcrA+DMraKcYnHr+wkEzcIphrqwPYYtUkwXUu712hHJ9x/vNtDzFQuauLUeA+ptFHFw2lHmozdT3bhXHDZ/cIHor49vtNzz2lCklfbUKldAVb+oJtjyIkYomskbDlCy4vZXVx0vp+oylazwMMsahdHePNjscxI9RqACLeSayUR0jVKD2MUY5l5PMZcr0s72E3UcftXEbi0XEXZDsJYnaqqVfJuaq+WET+kR3HaZhEXMviI7ztYzBFed8wUjCA9NHjTO/bEJClrbENsYdayWsyRioETLr3lrrKwXMrMR94olF3ErAg+JrsLSp94ZlNd816stU7UWjvF/4uYciWunmULYxSVDVzUAlfRbC2UI0hTKzbxUXDucnT8QQDDSF6ZiErcBjFOXn6lVqhy2g521x7wkBF7TgOIlGeiReSWBX9rg5sORLIg1iLNwKI0plZjRK30B9IwezG/wBDcKtB4sYyDmZIqYeY2GJb6K7BCbtIuCF8lXM7kjtFy3MZFKHOp3DvLO7LPiWdyWd+lsbc7DH3jtH1O42l5iWwaYFcssL7S8C5duUDtdcQ6I15ZhxV8vEBBbfMrl7ClmN3Zu1n+yV6UGbEo4vxKh4RIgTv7dbOYANwL867ynXUs/olHi7kUxDw8SoRE4uHiLU8rn0/99Xgx838zlNzXuzvfk+YFcZv1KxKhPdplQsx6zF0tVKQK5zuJtoy7+Mt14/mPVa4osV3d4GBAtZKkAksl2YaLzDzcPzD+UoIbSPmbZs94ROk/sUKlY+kYVF9EgNqaPmG+ihhcu0fsC4rDm0WPoJgyipkA5XL0rmKCMzL/SyjahS8KuYRLeL5mxFC+vpHyEom8z/Qr6Lx4ivqOIRR9hj1kZ2LwND4um00iwimpmQlTHFDyehd9LicdCdTE4dTH7lwsxr4me6KD4cda0W4xxHUfQZZonCPTbR0l73KG4RnuIXH01//2gAMAwAAARECEQAAEK1bn84SbnHhfI5/tF1PPGPBshCH0nPPQzwRIxh/PPrdSsoatPKGcj/xXCkZGDI1NAhZdPPG2Nt4fGtfPE3Jjo5GzNeNrECPIECfx//EAB4RAQEBAAMAAwEBAAAAAAAAAAEAERAhMSBBUTBh/9oACAECEQE/EBNpO3EF+PDJwn1GmAGQMqT904vU9Txscunu0sg8WBOF5Z7z6mYXqfbRzsQDC0YMHGy8uWcFaZ3cge0h3yx+r8bQ1tfCxNnR3PY+/B1ZEJt5yIay3b18COzS/wAyIyvW1nt38c/0Rn7GPp5SHT+RLf/EAB0RAQEBAAMBAQEBAAAAAAAAAAEAERAhMUEgMFH/2gAIAQERAT8QW7Zy2WGyC6my77GfZZ20g2BZ13DnJJwNv8Zotl3wznEwKm6TYbFrHmpM1kcmrup92wRnrC8I+/nesYaF27kvsM/DX2cT0vPLUxlnQjlZnRyC+wEEAwjPsjNPzv8Apdnkr5VDH+JxL//EACcQAQACAgIBAwUBAQEBAAAAAAEAESExQVFxYYGRECChscHR4fDx/9oACAEAAAE/EEOKglrLsYUJf0F8dzCOlTCQSYOSw+TXuhK46R9zS+zKvDoq8pkiKpdrXtAhL0Airtf8lTUvbQ/kqNhmnCEVDjAPUpdpH6FHiNpmMAZSpGeT9D7qWpxBKtq6nB4JRaltLK8EtUc52veBNR1AGxTVz8IiFqEXPS/5E0CGW5YDHTPD/JmCfB/Japc2WYMS8InHU9ZA5vMqgkzmfEvGJhIW29/VbLIG3wTLJFyP0cQzK+RW1lHH4lABZZDqFB1mmC13EKyt8otYGsl688Szbd84fqXPYFCXky+jt957GybeVlSvNjh3cobsQuGQBpM2paMQ0A+oI4seGK34XEvAGW2FfEMi1c4Zm0nqAgyntFo8ktp8mz9weVm/f/kXQ85WiNkpnlqKCF0Vh613FDraLPxBCprK+VwmLQWn0dep9Knf2KA4LDnCDFPd4SHnXXpEPJbQ6mVBTu3PzLXFO7bJ6V65TbpbolQq6lMdzAsqZIE67gfN/BA7DpOGNt2sB2OP7NR5OPtQnaIB8QagQtoof7EwHV5lVbfeCFwNA8kDK6/UojYlqcsCUI+YBda7lgolQCA0iED2wBW5SnkyjoS4UF6/9zf21sFKmlmm+SFrYxt+hEeNdKFxxF/TVoKLaD1l8lIHLMTYLXUAqOIPwM4oJBYB8v8AsChCGcQkLU4QukY+ka72ME9eJheYlkrJXW4cUKDAfaIlMB+IBFwKj3KUHPD6hLqBGTNqGAzBktt9j9ysaHkiDUsQE6w58y7G0bbgtxqCGzWmOqw1fmU2ACi0CXj3ukBjLwldrhF1jdb4Av2fux0CayeOyfjD4Iy3c6+zFKov4uJswPWNGgnMVKAdFS2bd+IMi45WjFHUFlVQlXivySrwOaQ6I20jTcYXsXG8ftgVUFB9oQFc+pkbaOB5X+TaVcv5L4jbDLS2G7Gj42/p1mOhblfGbhC1+IBhREgLXjiBjiLbgfWpSEFjYSCmGdyjqDGQ6eIt5UK+I0Fa5fk/MDQm+5f2+KkIF3sj2f5OKaqVvn/sBFCrbebv+xCYUm+BFRFoM5GpZRauMbI2lcmAUHwBZ51B0MpLMN9yhmuZW/mM3MRGqDrrbAmcNDEVHyriyfomasnmZqpfn6ZMLyoTMwLDY9J6QXwb4mCo+EBwVRK05qWyYUq/ETWrXpVnXcHEOKiXmjRmLU8gNvW4DYaQb/yFDzaiCUEsDeJqXi8RV5HvtcfgikwXjcsNbYzgP6MoaA9vsrN8wWxLrGvibaBV3hM2aOZ9ItoDnMFimBKJFD/3UeGddzAYUo7SgoYVXMs8I2DEogs6AH8TGnIbHhBwoYt9DuIFxAfA/ErF1TLTcUj1/wAPuPHsUEWM+5aKZIMBcb2o9Qgjeo8rL+42mwxn/wCZeJVslaLGFjyxPSUxB7ET7YlmRYeHgQEEDd1iOnuA5geVlfkPut9MwOaOYFd5cyhLCR+YFDOMTe6yXy3ut/UAAG8DDFCsxbL6JKsbzFybq6f+JhLlD7dHjOoS2NE9FBg5QDaR0/ap/SQ7Ud2duZRTdIxCLAPQwwNS3aEvu67pmGOXJqGNTnYoNzIeW9vxFhW9ym0qyQxk2rMNessdrXTW3R4f39tQRxxML1xFVHpUNXLRlhf7oABRMJLRGn9yzoepWdeCGaE2NPL/ACPN619aj0Da/MQFatdekpJAbE3cBdGlHR3879/saI7Sk3vMvHtHPmO16g2iaTUwBmDTlcWwASYTd9516sGnvMhLPTElNualB8RxOXviZj/2m+S/j6rU9rhmSOiLWnEcCUfPcQ+o2dwyPrBQjxcVE4/qajuYy6lJVnJ0sTHCk8j8P0WXjwShXaEzXxFkYQXETxN8Q8VPaEGyVAaji/UuChkmlcp8EIS0ugiEKTM9MqjRfIghmWMeGf/Z";
            
            isBase64Valid($userMaster['file']);
          
            $image = $this->api_model->saveProfileImage($userMaster);
            if(!empty($image))
            {
              $data['data'] = array(array('is_uploaded'=>time()));
              generateServerResponse('1','S', $data);
            }
            else
            {
              $data['is_uploaded'] = 0;
              generateServerResponse('0','F', $data);
            }
        }
        else
        {
          // $this->api_model->register($userMaster); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      } 
      // generateServerResponse('0','119');
    }

    /**
     * GET USER PROFILE DATA
     * @param   array
     * @return  Json Object
     */
    public function getProfileData()
    {
     $request_data  = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents('php://input');        
     $requestJson   = json_decode($request_data,true);  
     $check_request_keys = array(
                            '0'   => 'user_id',
                            '1'   => 'device_id',
                            '2'   => 'auth',
                            '3'   => 'timestamp'
                          );
      $resultJson    =  validateJson($requestJson, $check_request_keys);
      // print_r($request_data);

      if($resultJson==1)
      { 
        $userMaster['device_id']    = trim($requestJson['blendin']['device_id']); 
        $userMaster['user_id']      = trim($requestJson['blendin']['user_id']); 
        $userMaster['auth']         = trim($requestJson['blendin']['auth']);
        $userMaster['timestamp']    = trim($requestJson['blendin']['timestamp']); 

        //Validate is Fields not blank
        isBlank($userMaster['auth'],      '0', '140'); 
        isBlank($userMaster['device_id'], '0', '133'); 
        isBlank($userMaster['user_id'],   '0', '125'); 
        
        $isExists = $this->api_model->isExists('user', '_id', $userMaster['user_id']);

        if($isExists==1)
        {  
          $this->api_model->getProfileData($userMaster);
        }
        else
        {
          generateServerResponse('0','125'); 
        }
      }
      else
      {
        generateServerResponse('0','100');
      } 
      // generateServerResponse('0','119');
    }
    
 
}
