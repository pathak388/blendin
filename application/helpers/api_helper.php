<?php 
	
/**
 * @param type $msg_code
 * @param type $res_code
 * @param type $message
 * Thsi Function is responsible for all type of messages 
 */    
	
	function user_last_seen_format($request_time){
		$time_middle = new DateTime($request_time, new DateTimeZone(date_default_timezone_get()));
		$time_middle->setTimeZone(new DateTimeZone($_COOKIE['bl_timezone']));
		return $time_middle->format('Y-m-d H:i:s');
	}

	function generate_unique_code(){
		return substr(str_shuffle("1234567890"),'0','4');	
	}

   function generateServerResponse($msg_code, $res_msg, $data = null){
        
        $getDateTime = getDateAndIp();
        $array['blendin'] = array();
        $resultMsg = Messages($res_msg);
        // $array['blendin']["res_code"] = $msg_code;
        $array['blendin']["stat"]  = $resultMsg;
        // $array['blendin']["sync_time"]= $getDateTime['date'];
        
        if(!empty($data)){
            foreach($data as $key=>$val){
                $array['blendin'][$key]  = $val;
            }
        }
        $str = json_encode($array, true);
        echo str_replace("null",'""', $str);
        exit;
    }


    function getDateAndIp(){
        $result = array();	
        $result['ip'] = $_SERVER['REMOTE_ADDR'];
        $result['date'] = time();
        $result['datetime'] = date('Y-m-d h:i:s');
        return $result ; 
    }
	
	
    function validateJson($requestJson, $check_request_keys){
        if($requestJson){
			$validate_keys      = array();
			
			foreach($requestJson['blendin'] as $key=>$val){
				$validate_keys[] = $key;
			}
			
			$result = array_diff($validate_keys,$check_request_keys);

			if($result){ 
				return "0";
			}else{
				return  "1";
			} 
		}else{
			return  "0";
		}	       
    }
    
    
	function validateEmail($email_a){
		if (!filter_var($email_a, FILTER_VALIDATE_EMAIL)) {
			return 0;
		}
	}
	
	function isBlank($fieldName, $msgCode, $msgType){
        if($fieldName == ''){
            generateServerResponse($msgCode, $msgType);
        }
    }
    
    function checkLength($fieldName, $fieldLength,$msgCode, $msgType){
		$length =  strlen($fieldName);
		if($length > $fieldLength){
		   generateServerResponse($msgCode, $msgType);
		}
    }
    
    function isPhone($fieldName, $msgCode, $msgType){
		if(!ctype_digit($fieldName)){
		  generateServerResponse($msgCode, $msgType);
		} 
	}
    
    function isDobBlank($fieldName, $msgCode, $msgType){
        if($fieldName == '' || $fieldName == '0000-00-00'){
            generateServerResponse($msgCode, $msgType);
        }
    }
    function isDobFormat($fieldName, $msgCode, $msgType){
		if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fieldName)){
			generateServerResponse($msgCode, $msgType);
		}
	}
    
	function isFutureDate($fieldName, $msgCode, $msgType){
		if($fieldName < mktime()){
			generateServerResponse($msgCode, $msgType);
		}
	}
	
	function phoneLength($fieldName, $msgCode, $msgType){
		if(strlen($fieldName) != 10){
			 generateServerResponse($msgCode, $msgType);
		}
	}
    
    function phoneMinLength($fieldName, $msgCode, $msgType){
		if(strlen($fieldName) < 9){
			 generateServerResponse($msgCode, $msgType);
		}
	}
	function phoneMaxLength($fieldName, $msgCode, $msgType){
		if(strlen($fieldName) >= 15){
			 generateServerResponse($msgCode, $msgType);
		}
	}
	 	
	function Messages($res_msg){
        $codes = Array(
                    '100' => 'Input json is not valid.',
                    '101' => 'Json Used is not valid.',
                    '102' => 'Please enter your username.',
					'103' => 'Username entered already exists.',
                    '104' => 'Please enter your email.',
					'105' => 'Email entered is not valid.',
                    '106' => 'Email entered already exists.',
					'107' => 'Please enter your password.',
					'108' => 'Type can not be empty',
					'109' => 'First Name can not be empty',
					'110' => 'Email can not be empty',
					'111' => 'Category can not be empty',
					'112' => 'Subject can not be empty',
					'113' => 'Message can not be empty',
					'114' => 'Article Id can not be empty',
					'115' => 'Comment Category can not be empty',
					'116' => 'Invalid Comment Category Id',
					'117' => 'Invalid Article Id',
					'118' => 'Invalid UUID',
					'119' => 'Comment Text can not be empty',
					'120' => 'Last Name can not be empty',
					'121' => 'Phone number can not be empty',
					'122' => 'Street can not be empty',
					'123' => 'City can not be empty',
					'124' => 'Zipcode can not be empty',
					'125' => 'userId can not be empty',
					'126' => 'Image can not be empty',
					'127' => 'Invalid case category Id',
					'128' => 'Invalid Email or Password',
					'129' => 'Invalid login id.',
					'130' => 'User already logged out.', 
					'131' => 'Invalid video Id.', 
					'132' => 'Invalid credentials.', 
					'133' => 'Device Id can not be empty.',
					'134' => 'app_open_for_day can not be blank.',
					'135' => 'Mobile number alredy registered.',
					'136' => 'File can not be empty.',
					'137' => 'base64 string is not valid.',
					'138' => 'msisdn can not be empty.',
					'139' => 'otp can not be empty.',
					'140' => 'Auth token can not be empty.',
					'141' => 'Invalid user ID and device Id.',
					'142' => 'Event type can not be empty.',
					'143' => 'Invalid event type.',
					'201' => 'Logged out successfully Done.',
					'W'   => 'Something went wrong.',
					'S'   => 'Success',
					'F'   => 'Fail',
					'E'   => 'Data Not Found.'
					                
                );

        return (isset($codes[$res_msg])) ? $codes[$res_msg] : '';        
    }
