<?php
class Api_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->helper('api');
		$this->load->database();
	
	}
	 
# APP OPEN REGION STARTS

    public function appStart($requestData)
    {
        $requestData['domain'] = DOMAIN;
        $requestData['phylum'] = PHYLUM;

        $this->db->insert('analytics', $requestData);
        if($this->db->insert_id() > 0)
        {
            generateServerResponse('0', 'S');
        }
        else
        {
            generateServerResponse('1', 'F');
        }
    }
# APP OPEN REGION ENDS

# USERS REGION STARTS

    /**
     * Get user by email
     */
    public function getUserByEmail($email) { 
        return $this->db->get_where('user', array('email' => $email))->row_array();
    }

    /**
     * Get user by _id
     */
    public function getUserById($id) {
        return $this->db->get_where('user', array('_id' => $id))->row_array();
    }

     
    /**
     * Getting all users
     */
    public function getAllUsers() {
        return $this->db->get('user')->result_array(); 
    }
 
    public function register($requestData, $type)
    {
        # insert data in user table
        $name                   = $requestData['name'];  
        $name                   = explode(' ', $name, 3); 
        $user['_id']            = md5($name.$requestData['email']); //substr(md5($name.$requestData['email']),  0, 6);
        $user['first_name']     = $name[0];
        $user['middle_name']    = (count($name) > 2 ? $name[1] : '');
        $user['last_name']      = (count($name) == 3 ? $name[2] : (count($name) ==2 ? $name[1] : ''));
        $user['mobile']         = $requestData['mobile'];
        $user['email']          = $requestData['email'];
        $user['date_of_birth']  = $requestData['date_of_birth'];
        $user['gender']         = $requestData['gender'];
        $user['foodi_type']     = $requestData['foodi_type'];
        $user['interest_list']  = $this->get_comma_separate_string($requestData['interest_list'], 'interest');
        $user['cuisine_list']   = $this->get_comma_separate_string($requestData['cuisine_list'], 'cuisine');
        $this->db->insert('user', $user);
        $user_id = $user['_id']; 
        // print_r($user); exit;

        # Save data in interest table 
        $this->save_interests($requestData['interest_list']);

        # Save data in dishes table (cusine)
        $this->save_dishes($requestData['cuisine_list']);

        # Save data in user_interest table
        $this->save_user_interests($user_id, $user['interest_list']);

        # Save data in user_metadata table
        $this->save_user_metadata($user_id, $requestData);

        # Save data in user_devices table
        $this->save_user_device($user_id, $requestData);
        $arr['user_id']     = $user_id;
        $arr['device_id']   = $this->last_device_id($user['mobile']);
        $arr['device_id']   = (!empty($data['device_id']) ? $data['device_id'] : $requestData['species']);
        $arr['user_tag']    = 'User Registered Successfully.';   
        $data['data']       = array($arr);
        generateServerResponse('1','S', $data);
    }

    public function update_user($requestData)
    {
        echo 'inside update user';
    }

    /**
     * SEND OTP 
     * @param   array
     * @return  Json Object
     */
    public function sendOtp($requestData)
    {
        # Save data in user_device
        $user_id            = $this->get_field_value('user', '_id', 'mobile', $requestData['mobile']);
        $ud['mobile']       = $requestData['mobile'];
        $ud['species']    = $requestData['device_id'];
        $this->save_user_device($user_id, $ud);

        # Save data in verify_otp
        $OTP                = generate_unique_code();
        $votp['otp']        = $OTP;
        $votp['msisdn']     = $requestData['mobile'];
        $votp['device_id']  = $requestData['device_id'];
        $votp['add_date']   = time();
        $votp['verify']     = '0';
        if($this->db->insert_id() > 0) { $this->db->insert('verify_otp', $votp); }

        # Send otp
        if($this->db->affected_rows() > 0)
        {
            $msg    = 'Your verification code for Blendin is '.$OTP;
            file("http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=GSCHAT&password=1234566&sender=GSCHAT&mobileno=".$requestData['mobile']."&msg=".urlencode($msg));
            $arr['otp'] = base64_encode($OTP);
            $data['data']       = array($arr);
            generateServerResponse('1','S', $data);
        }
        else
        {
            generateServerResponse('0','F');
        }
    }

    /**
     * SEND OTP 
     * @param   array
     * @return  Json Object
     */
    public function verifyOtp($requestData)
    {
        $this->db->order_by('_id', 'DESC');
        $this->db->limit(1,0);
        $where = array(
                    'otp'       => $requestData['otp'],
                    'msisdn'    => $requestData['mobile'],
                    'device_id' => $requestData['device_id'],
                    'verify'    => '0'
                    );
        $result = $this->db->get_where('verify_otp', $where)->row_array();
        // print_r($result);
        if($result)
        {
            # Update verify_otp set verify = 1
            $this->db->where('_id', $result['_id']);
            $this->db->update('verify_otp', array('verify'=>'1'));
            $data['data'] = array(array('msisdn'=>$requestData['mobile']));
            generateServerResponse('1','S', $data);
        }
        else
        {
            generateServerResponse('0','F');
        }
    }

    /**
     * SEND OTP 
     * @param   array
     * @return  Json Object
     */
    public function signUpAttempt($requestData)
    {
        # Save data in analytics
        $analytics['genus']     = $requestData['mobile'];
        $analytics['species']   = $requestData['species'];
        $analytics['domain']    = 'signup_attempt';
        $analytics['add_date']  = time();
        $this->db->insert('analytics', $analytics);

        # Save data in user_device
        $user_id = $this->get_field_value('user', '_id', 'mobile', $requestData['mobile']);
        $this->save_user_device($user_id, $requestData);
 
        if($this->db->affected_rows() > 0)
        { 
            generateServerResponse('1','S');
        }
        else
        {
            generateServerResponse('0','F');
        }
    }

    public function getProfileData($requestData)
    {
        $this->get_user_profile($requestData['user_id']);
    }
# USERS REGION ENDS

# EVENT REGION STARTS

    public function postEvent($requestData)
    { 
        # Save data in events table
        $event_id = $this->save_event($requestData); 
        if($event_id > 0)
        {
            # Save data in meal table
            $this->save_meal($requestData);

            # Save data of cuisine_list in dishes table
            $this->save_dishes_and_type($requestData['cuisine_list']); 

            # Save data of drink_list in dishes table
            foreach ($requestData['drink_list'] as $key => $value) {
               $dlist[]['cuisine'] = $value['drink'];
            }   
            $this->save_dishes($dlist);

            $arr['event_id']    = $event_id;
            $arr['event_score'] = '';
            $arr['tag_list']    = $requestData['tag'];
            $response['data']   = $arr;
            generateServerResponse('0', 'S', $response);
        }
        else
        {
            generateServerResponse('0', 'F');
        } 
    }

    public function submit_review_guest($requestData)
    {
        $this->db->insert('event_guest', $requestData);
        if($this->db->affected_rows() > 0)
        {
            $data['data'] = $requestData['message'];
            generateServerResponse('1','S', $data);
        }
        else
        {
            generateServerResponse('0','F');
        }
    }

    public function submit_review_host($requestData)
    {
        $this->db->insert('event_guest', $requestData);
        if($this->db->affected_rows() > 0)
        {
            $data['data'] = $requestData['message'];
            generateServerResponse('1','S', $data);
        }
        else
        {
            generateServerResponse('0','F');
        }
    }
# EVENT REGION ENDS

# FUNCTIONS REGION STARTS
 
    public function save_event($requestData)
    {   
        $event['host_id']       = $requestData['user_id'];
        $event['venue_id']      = $requestData['venue_id'];
        $event['guest_count']   = $requestData['max_guest'];
        $event['guest_limit']   = $requestData['min_guest'];
        $event['fee']           = $requestData['cost'];
        $event['time']          = $requestData['time'];
        $event['date']          = $requestData['date'];
        $event['title']         = $requestData['title'];
        $event['description']   = $requestData['description'];
        $event['image_count']   = $requestData['image_count'];
        $event['event_type']    = $requestData['event_type'];
        $event['tag']           = get_array_to_json_string($requestData['tag']);
        $event['interest_list'] = get_array_to_json_string($requestData['interest_list']);
        $event['guest_list  ']  = get_array_to_json_string($requestData['guest_list']);
        $event['cuisine_list']  = get_array_to_json_string($requestData['cuisine_list']);
        $event['drink_list']    = get_array_to_json_string($requestData['drink_list']);
        $this->db->insert('events', $event);
        return $this->db->insert_id();
    }

    public function save_meal($requestData)
    {
        $type = array();
        foreach ($requestData['cuisine_list'] as $key => $value) { $type[] = $value[key($value)]; } 
        $meal['is_veg']         = (!in_array('non-veg', $type) ? 1 : 0);
        $meal['cuisine_type']   = $requestData['cuisine_type'];
        $meal['cooked_by']      = $requestData['cooked_by']; 
        $meal['host_id']        = $requestData['user_id'];
        $meal['dish_list']      = get_array_to_json_string($requestData['cuisine_list']);
        $meal['dessert_list']   = get_array_to_json_string($requestData['drink_list']);
        $meal['about_menu']     = $requestData['about_menu'];
        $this->db->insert('meal', $meal);
        return $this->db->insert_id();
    }

    public function save_dishes($requestData)
    {
        foreach ($requestData as $value) 
        {
            $exists = $this->isExists('dishes', 'name', trim($value['cuisine']));
            # if Cuisine already exists increment count else add insert new interest
            if($exists==1)
            {
                // $sql = "UPDATE dishes SET popularity=popularity+1 WHERE name='$value[cuisine]'";
                $sql = "UPDATE dishes SET popularity=popularity+1 WHERE name='$value[cuisine]' AND type='".VEG."'";
                $this->db->query($sql);
            }
            else
            {
                $this->db->insert('dishes', array('name'=>$value['cuisine'], 'popularity'=>1, 'type'=>VEG));
            }
        }
    }

    public function save_dishes_and_type($requestData)
    {
        foreach ($requestData as $value) 
        { 
            $name   = trim(key($value));
            $type   = trim($value[key($value)]);
            $exists = $this->db->get_where('dishes', array('name'=>$name, 'type'=>$type))->num_rows(); 
            // echo $this->db->last_query();
            $exists = $this->isExists('dishes', 'name', trim(key($value)));
            # if Cuisine already exists increment count else add insert new interest
            if($exists > 0)
            {
                $sql = "UPDATE dishes SET popularity=popularity+1 WHERE name='$name' AND type='$type'";
                $this->db->query($sql);
            }
            else
            {
                $this->db->insert('dishes', array('name'=>$name, 'popularity'=>1, 'type'=>$type));
            }
        }
    }

    public function getRemoteImage($image_url, $folder)
    { 
        if(!empty($image_url))
        {
            $image = '';
            $imagedata  = file_get_contents($image_url); 
            $base64     = base64_encode($imagedata);
            /**/
            $img = imagecreatefromstring(base64_decode($base64)); 
             
            if($img != false) 
            {  
                $imageName = time().'.jpg';

                $path = $_SERVER['DOCUMENT_ROOT'];
                $path = $path.'/evolution/assets/images/'.$folder.'/'.$imageName;
                if(imagejpeg($img, $path)) 
                {  
                    $image=$imageName;
                }
                else
                { 
                    $image="";  
                }                                        
            }
            return $image;
        }
        else
        {
            return 'default.png';
        }
    } 

    public function isExists($table, $field, $value)
    {
        $count = $this->db->get_where($table, array($field=>$value))->num_rows();
        return ($count > 0 ? 1 : 0);
    }

    public function is_user_registered($email, $mobile)
    {
        $count = $this->db->get_where('user', array('email'=>$email, 'mobile'=>$mobile))->num_rows();
        return ($count > 0 ? 1 : 0);
    }

    public function validate_user_device($requestData)
    {
        $this->db->select('u.mobile, ud.device_id');
        $this->db->from('user AS u');
        $this->db->join('user_devices AS ud', 'u.mobile = ud.msisdn', 'left');
        $this->db->where(array('u.mobile' => $requestData['mobile'], 'ud.device_id' => $requestData['device_id']));
        $count = $this->db->get()->num_rows();
        return ($count > 0 ? 1 : 0);
    }

    public function get_field_value($table, $field, $where_column, $value)
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where($where_column, $value);
        $result = $this->db->get()->row_array();
        return $result[$field];
    }

    /**
     * GET comma separated string from 2D array to 1D array
     * @param   2D array
     * @return  1D array
     */
    public function get_comma_separate_string($arr2D, $field)
    {
        $string = '';
        foreach ($arr2D as $value) {
           $string .= $value[$field].',';
        }
        return rtrim($string,',');
    }

    /**
     * SAVE IMAGE TO DIRECTORY
     * @param   base64 string
     */  
    public function saveProfileImage($requestData)
    {
        $user_id        = $requestData['user_id'];
        $profile_pic    = $requestData['file'];
        $location       = 'profilepic';

        # Save data in user_device 
        $ud['mobile']   = $this->get_field_value('user', 'mobile', '_id', $user_id);
        $ud['species']  = $requestData['device_id'];
        $this->save_user_device($user_id, $ud);

        #Save profile picture in folder
        return saveProfileImage($user_id, $profile_pic, $location);
    }

    public function save_user_interests($user_id, $interests)
    {
        if($interests)
        {
            $interests = explode(',', $interests);
            $array['user_id']        = $user_id;
            $array['direct_score']   = 0;
            $array['inderect_score'] = 0;
            foreach ($interests as $value) 
            {
                $array['interest_id'] = $this->get_interest_id_by_name($value);
                $this->db->insert('user_interest', $array);
            }
        }
    }

    public function get_interest_id_by_name($name)
    {
        $this->db->select('_id');
        $this->db->from('interests');
        $this->db->where('name', $name);
        $result = $this->db->get()->row_array();
        return $result['_id'];
    }

    public function save_interests($requestData)
    {
        foreach ($requestData as $value) 
        {
            $exists = $this->isExists('interests', 'name', trim($value['interest']));
            # if interest already exists increment count else add insert new interest
            if($exists==1)
            {
                $sql = "UPDATE interests SET population_count=population_count+1 WHERE name='$value[interest]'";
                $this->db->query($sql);
            }
            else
            {
                $this->db->insert('interests', array('name'=>$value['interest'], 'population_count'=>1));
            }
        }
    }

    public function save_user_metadata($user_id,$requestData)
    {
        $array['_id']       = $user_id;
        $array['about_me']  = $requestData['about_me'];
        $array['education'] = $requestData['education'];
        $array['profession']= $requestData['work'];
        $this->db->insert('user_metadata', $array);
    }

    public function save_user_device($user_id, $requestData)
    {
        $data['device_id']      = $requestData['species'];
        $data['msisdn']         = $requestData['mobile'];
        $data['username']       = $this->get_field_value('user', 'username', '_id', $user_id);
        $data['last_login_time']= time();
        $where = array('msisdn'=>$data['msisdn'], 'device_id'=>$data['device_id']);
        $count = $this->db->get_where('user_devices', $where)->num_rows();

        if($count > 0)
            $this->db->update('user_devices', $data, $where);
        else
            $this->db->insert('user_devices', $data);
    }

    public function last_device_id($mobile){
        return $this->get_field_value('user_devices', 'device_id', 'msisdn', $mobile);
    }

    public function get_user_profile($user_id)
    {
        $this->db->select('u._id, CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as name, 
                            u.address, u.gender, u.interest_list, u.cuisine_list, u.foodi_type, 
                            um.about_me, um.profession as work, um.education');
        $this->db->from('user as u');
        $this->db->join('user_metadata as um', 'um._id = u._id', 'left');
        $this->db->where('u._id', $user_id);
        $result = $this->db->get()->row_array();

        $result['thumbnail']            = $this->get_user_pic($user_id, 'thumbnail');
        $result['gathering_attended']   = $this->event_attended($user_id);
        $result['gathering_hosted']     = $this->event_hosted($user_id);
        $result['contact_list']         = '';
        $result['contact_or_not']       = '';
        $result['timestamp']            = time();
        echo json_encode($result);
        // print_r($result);
    }
    
    public function get_user_pic($user_id, $imageType='')
    {
        $DR = $_SERVER['DOCUMENT_ROOT'].'/blendin/';
        $p2 = 'assets/images/profilepic/'.$imageType.'/'.$user_id.'.jpg';
        // echo $DR.$p2;
        if(!empty($imageType))
        {
            if (file_exists($DR.$p2)) {
                // return site_url().$p2;
                $data['data'] = site_url().$p2;
                generateServerResponse('1','S', $data); 
            } else {
                // return '';
                generateServerResponse('0','144'); 
            }
        }
        else
        {
            # return array of images from all folders
        }        
    }

    public function event_attended($user_id)
    {
        $this->db->select('_id as event_id');
        $this->db->from('event_guest');
        $this->db->where('guest_id', $user_id);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function event_hosted($user_id)
    {
        $this->db->select('_id as event_id');
        $this->db->from('events');
        $this->db->where('host_id', $user_id);
        $result = $this->db->get()->result_array();
        return $result;
    }
# FUNCTIONS REGION STARTS
 
 

}
?>
